<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use AWS;

class LexController extends Controller
{
    public function index(Request $request){
        $status  = false;
        $message = '';
        $data    = [];

        if($request->userid) {
            if($request->message){
                $client = AWS::createClient('LexRuntimeService');
                $result = $client->postText([
                    'botAlias'  => 'BotProfileAlias', // REQUIRED
                    'botName'   => 'BotProfile', // REQUIRED
                    'userId'    => $request->userid, // REQUIRED
                    'inputText' => $request->message, // REQUIRED
                    'requestAttributes' => array(),
                    'sessionAttributes' => array(),      
                ]);

                $response = $result->toArray();

                if($response['message']){
                    $status = true;
                    $data   = [
                        "slots"   => !empty($response['slots']) ? $response['slots'] : [],
                        "message" => !empty($response['message']) ? $response['message'] : '' ,
                        "dataFromLexRuntimeService" => $response,
                    ];
                }
            }
        } else {
            $message = 'userId notfound';
        }

        $data_res = [
            'status'  => $status,
            'message' => $message,
            'data'    => $data,
        ];

        return response()->json($data_res, 200);
    }
}
