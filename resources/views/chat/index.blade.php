@extends('layout.index')

@section('css')
    <style>
        .chat-typing{
            float: left;
            font-size: 15px;
            color: #000;
            padding: 10px;
            max-width: 80%;
            border-radius: 15px;
            background-color: #F8F9FA; 
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .chat-left{
            float: left;
            font-size: 15px;
            color: #000;
            padding: 10px;
            max-width: 80%;
            border-radius: 15px;
            background-color: #DADADA; 
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .chat-right{
            float: right;
            font-size: 15px;
            color: #000;
            padding: 10px;
            max-width: 80%;
            border-radius: 15px;
            background-color: #CDE1E7;
            margin-top: 0px;
            margin-bottom: 0px;
        }

        .chat-item{
            padding-top: 0px; 
            padding-bottom: 0px;
            margin-top: 60px;
            margin-bottom: 120px;
        }
    </style>
@endsection

@section('content')
    <div id="chat">
        <div class="container-sm">
            <header class="d-flex p-3 bg-light fixed-top text-muted">
                <span>Botlex</span>
            </header>

            <div class="chat-item">
                <ul class="list-group">
                    <li class="list-group-item" style="border: 0px !important;" v-for="item in chatList">
                        <span class="chat-left" v-html="item.message" v-if="item.type == 'bot'"></span>
                        <span class="chat-right" v-html="item.message" v-if="item.type == 'me'"></span>
                    </li>

                    <li class="list-group-item" style="border: 0px !important;" v-if="loading">
                        <span class="chat-typing">Typing . . .</span>
                    </li>
                </ul>
            </div>

            <footer class="d-flex p-1 bg-light fixed-bottom">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Type message" v-model="form.message" v-on:keyup.enter="sendMessage()">

                    <button class="btn btn-primary" type="button" @click="sendMessage()">
                        <i class="fas fa-paper-plane"></i>
                    </button>
                </div>
            </footer>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript" async src="https://tenor.com/embed.js"></script>

	<script>
    	var vue = new Vue({
			el: '#chat',
      		data: function(){
          		return {
          			loading:false,
          			chatList:[],
                    form:{
                        userid:Math.random().toString(36).slice(2),
                        message:''
                    },
        		}
      		},
      		mounted: function() {
        		this.sendMessage();
      		},
      		methods: {
          		sendMessage: function(){
            		var url = apiEndpoint + '/' + 'lex';

                    console.log('cek cek url', url);
                    console.log('cek cek form', this.form);
                    
                    if(this.form.message){
                        this.chatList.push(
                            {
                                type:'me',
                                message:this.form.message
                            }
                        );
                        
                        const form = {
                            userid:this.form.userid,
                            message:this.form.message,
                        }; 

                        this.form.message = '';
                        this.loading      = true;
                        window.scrollBy(0, 300);
                        
                        axios.post(url, form)
                        .then(response => {
                            var temp = response.data;
                            console.log('cek cek ', temp);

                            this.chatList.push(
                                {
                                    type:'bot',
                                    message:temp.data.message
                                }
                            );  

                            window.scrollBy(0, 300);
                        })
                        .catch(error => {
                            console.log(error)
                        })
                        .finally(() => this.loading = false);
                    }
          		},
      		}
    	})
  	</script>
@endsection