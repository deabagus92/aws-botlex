<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Botlex Test</title>
    
    <link rel="stylesheet" type="text/css" href="{{asset('assets/bootstrap/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/fontawesome/css/all.css')}}">
    
    @yield('css')
</head>

<body>
  	@yield('content')
	
	<script type="text/javascript" src="{{asset('assets/bootstrap/js/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/vue.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/axios.min.js')}}"></script>

    <script>
		var apiEndpoint = "{{ env('API_ENDPOINT') }}"+"/api";
	</script>

	@yield('js')
</body>
</html>